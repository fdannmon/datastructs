#include <stdio.h>
#include <stdlib.h>

#include "node.h"

class LinkedList
{
  public:
    LinkedList();
    void addAtBeginning(int number);
    void showList();
  private:
    Node *header;
    Node *last;
};

LinkedList::LinkedList()
{
  header = NULL;
  last = NULL;
}

void LinkedList::addAtBeginning(int number)
{
  Node *newNode;
  newNode = (Node*) malloc(sizeof(Node));
  newNode->data = number;
  newNode->next = NULL;

  if(header == NULL)
  {
    header = newNode;
  } else {
    newNode->next = header;
    header = newNode;
  }
}

void LinkedList::showList()
{
  Node *aux;
  aux = header;
  if(aux != NULL)
  {
    while(aux != NULL)
    {
      printf("%d ", aux->data);
      aux = aux->next;
    }
    printf("\n");
  } else {
    printf("La lista está vacía.\n");
  }
}



