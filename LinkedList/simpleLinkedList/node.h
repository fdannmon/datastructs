/* 
 * Author: Danny Chucos
 *
 */

struct Node
{
  int data;
  Node *next;
};
