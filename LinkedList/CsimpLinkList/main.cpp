#include <stdio.h>
#include <stdlib.h>
#include "node.h"

void createList(Node **init);
void addAtLeft(Node **header, int number);
void addAtRight(Node **header, int number);
void deleteByOrder(Node **header, int order);
void deleteByElement(Node **header, int elem);
void showList(Node **header);

int main()
{
  Node *newList;
  createList(&newList);

  addAtLeft(&newList, 2);
  addAtLeft(&newList, 1);
  addAtLeft(&newList, 0);

  addAtRight(&newList, 3);
  addAtRight(&newList, 2);
  addAtRight(&newList, 4);
  addAtRight(&newList, 2);
  addAtRight(&newList, 5);
  addAtRight(&newList, 2);

  deleteByOrder(&newList, 0);
  showList(&newList);
  deleteByElement(&newList, 2);
  showList(&newList);

  //system("pause"); uncomment for Windows
  return 0;
}

void createList(Node **init)
{
  *init = NULL;
}

void addAtLeft(Node **header, int number)
{
  Node *p;
  p = (Node*) malloc(sizeof(Node));
  p->data = number;
  p->next = NULL;

  if(*header == NULL)
  {
    *header = p; 
  } else {
    p->next = *header;
    *header = p;
  }
}

void addAtRight(Node **header, int number)
{
  Node *aux, *h;
  h = *header;
  aux = (Node*) malloc(sizeof(Node));
  aux->data = number;
  aux->next = NULL;

  if(*header == NULL)
  {
    *header = aux;
  } else {
    while(h->next != NULL)
    {
      h = h->next; 
    }
    h->next = aux;
  }
}

void deleteByOrder(Node **header, int order)
{
  Node *before, *h;
  h = *header;
  int countOrder = 0;
  
  if(h == NULL)
  {
    printf("La lista está vacía.\n");
  } else {
    while(h != NULL)
    {
      if(countOrder == order)
      {
        if(h == *header) //it analyzes if the order was 0
        {
          *header = (*header)->next;
        } else {
          before->next = h->next;
        }
        free(h);
        break;
      } else {
        before = h;
        h = h->next;
        countOrder++;
      }
    }
  }
}

void deleteByElement(Node **header, int elem)
{
  Node *before, *h;
  h = *header;
  
  if(h == NULL)
  {
    printf("La lista está vacía.\n");
  } else {
    while(h != NULL)
    {
      if(h->data == elem)
      {
        if(h == *header) //it analyzes if elem is at first position
        {
          *header = (*header)->next;
        } else {
          before->next = h->next;
        }
        free(h);
      } else {
        before = h;
        h = h->next;
      }
    }
  }
}

void showList(Node **header)
{
  Node *h;
  h = *header;
  
  if(*header == NULL)
  {
    printf("La lista está vacía.\n");
  } else {
    while(h != NULL)
    {
      printf(" %d ", h->data);
      h = h->next;
    }
    printf("\n");
  }
}

