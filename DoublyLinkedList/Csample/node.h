struct Node
{
  int data;
  Node *previous;
  Node *next;
};
