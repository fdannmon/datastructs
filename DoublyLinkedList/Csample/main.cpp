#include <stdio.h>
#include <stdlib.h>
#include "node.h"

void createList(Node**);
void insertAtBegin(Node**, int);
void insertAtEnd(Node**, int);
void insertAtIndex(Node**, int, int);
void deleteFirst(Node**);
void deleteLast(Node**);
void deleteByIndex(Node**, int);
void showList(Node**);
void showReverse(Node**);

int main()
{
  Node *head1;

  createList(&head1);
  showList(&head1);

  insertAtBegin(&head1, 1);
  insertAtBegin(&head1, 2);
  insertAtEnd(&head1, 3);
  insertAtEnd(&head1, 4);
  insertAtEnd(&head1, 5);
  insertAtEnd(&head1, 6);
  insertAtEnd(&head1, 7);
  insertAtIndex(&head1, 8, 4);
  showReverse(&head1);

  insertAtBegin(&head1, 10);
  insertAtBegin(&head1, 11);
  insertAtBegin(&head1, 12);
  insertAtBegin(&head1, 13);
  insertAtBegin(&head1, 14);
  deleteByIndex(&head1, 0);
  showList(&head1);

  deleteFirst(&head1);
  showList(&head1);
  
  deleteLast(&head1);
  showList(&head1);
  
  deleteByIndex(&head1, 4);
  showList(&head1);

  return 0;
}


void createList(Node **header)
{
  *header = NULL;
}

void insertAtBegin(Node **header, int number)
{
  Node *q;
  q = (Node*) malloc(sizeof(Node));
  q->data = number;
  q->previous = NULL;
  q->next = NULL;

  if(*header == NULL)
  {
    *header = q;
  } else {
    (*header)->previous = q;
    q->next = *header;
    *header = q;
  }
}

void insertAtEnd(Node **header, int number)
{
  Node *q, *aux;
  aux = *header;
  q = (Node*) malloc(sizeof(Node));
  q->data = number;
  q->previous = NULL;
  q->next = NULL;

  if(aux == NULL)
  {
    printf("List is empty.\n");
  } else {
    while(aux->next != NULL)
    {
      aux = aux->next;
    }
    aux->next = q;
    q->previous = aux;
  }
}

void insertAtIndex(Node **header, int number, int index)
{
  Node *q, *aux;
  aux = *header;
  q = (Node*) malloc(sizeof(Node));
  q->data = number;
  q->previous = NULL;
  q->next = NULL;

  if(aux == NULL)
  {
    *header = q;
  } else {
    int countIndex = 0;
    while(aux != NULL)
    {
      if(countIndex == index)
      {
        if(aux == *header)
        {
          aux->previous = q;
          q->next = *header;
          *header = q;
        } else {
          q->previous = aux->previous;
          q->next = aux;
          aux->previous->next = q;
          aux->previous = q;
        }
        break;
      } else {
        aux = aux->next;
        countIndex++;
      }
    }
  }
}

void deleteFirst(Node **header)
{
  Node *aux;
  aux = *header;

  if(*header == NULL)
  {
    printf("List is empty.\n");
  } else {
    *header = (*header)->next;
    (*header)->previous = NULL;
    free(aux);
  }
}

void deleteLast(Node **header)
{
  Node *aux;
  aux = *header;

  if(*header == NULL)
  {
    printf("List is empty.\n");
  } else {
    while(aux->next != NULL)
    {
      aux = aux->next;
    }
    aux->previous->next = NULL;
    free(aux);
  }
}

void deleteByIndex(Node **header, int index)
{
  Node *aux;
  aux = *header;
  
  if(aux == NULL)
  {
    printf("List is empty.\n");
  } else {
    int countIndex = 0;
    while(aux != NULL)
    {
      if(countIndex == index)
      {
        if(aux == *header)
        {
          *header = (*header)->next;
        } else {
          aux->previous->next = aux->next;
          aux->next->previous = aux->previous;
        }
        free(aux);
        break;
      } else {
        aux = aux->next;
        countIndex++;
      }
    }
  }
}

void showList(Node **header)
{
  Node *aux;
  aux = *header;

  if(aux == NULL)
  {
    printf("List is empty.\n");
  } else {
    while(aux != NULL)
    {
      printf(" %d ", aux->data);
      aux = aux->next;
    }
    printf("\n");
  }
}

void showReverse(Node **header)
{
  Node *aux;
  aux = *header;

  if(aux == NULL)
  {
    printf("List is empty.\n");
  } else {
    while(aux->next != NULL)
    {
      aux = aux->next;
    }
    while(aux != NULL)
    {
      printf(" %d ", aux->data);
      aux = aux->previous;
    }
    printf("\n");
  }
}


