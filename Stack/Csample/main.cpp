#include <stdio.h>
#include <stdlib.h>
#include "node.h"

void createStack(Node**);
void push(Node**, int);
void pop(Node**);
void showStack(Node**);

int main()
{
  Node *newStack;
  createStack(&newStack);
  push(&newStack, 1);
  push(&newStack, 2);
  push(&newStack, 3);
  showStack(&newStack);
  pop(&newStack);
  showStack(&newStack);

  return 0;
}

void createStack(Node **header)
{
  *header = NULL;
}

void push(Node **header, int number)
{
  Node *q;
  q = (Node*) malloc(sizeof(Node));
  q->data = number;
  q->next = NULL;
  
  if(*header == NULL)
  {
    *header = q;
  } else {
    q->next = *header;
    *header = q;
  }
}

void pop(Node **header)
{
  Node *aux, *before;
  aux = *header;

  if(*header == NULL)
  {
    printf("La pila está vacía.\n");
  } else {
    while(aux->next != NULL)
    {
      before = aux;
      aux = aux->next;
    }
    before->next = NULL;
    free(aux);
  }
}

void showStack(Node **header)
{
  Node *aux;
  aux = *header;

  if(*header == NULL)
  {
    printf("La lista está vacía.\n");
  } else {
    while(aux != NULL)
    {
      printf(" %d ", aux->data);
      aux = aux->next;
    }
    printf("\n");
  }
}

